/*
 * Public API Surface of ng-exames
 */

export * from './lib/ng-exames.service';
export * from './lib/ng-exames.component';
export * from './lib/ng-exames.module';
