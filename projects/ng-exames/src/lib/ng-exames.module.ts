import { NgModule } from '@angular/core';
import { NgExamesComponent } from './ng-exames.component';



@NgModule({
  declarations: [NgExamesComponent],
  imports: [
  ],
  exports: [NgExamesComponent]
})
export class NgExamesModule { }
