import { TestBed } from '@angular/core/testing';

import { NgExamesService } from './ng-exames.service';

describe('NgExamesService', () => {
  let service: NgExamesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgExamesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
