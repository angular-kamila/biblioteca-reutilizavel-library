import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgExamesComponent } from './ng-exames.component';

describe('NgExamesComponent', () => {
  let component: NgExamesComponent;
  let fixture: ComponentFixture<NgExamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgExamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgExamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
