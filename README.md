# NgExamesApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.
Criando módulo para ser utilizado em diversos projetos (estrutura).

## Developer
[Kamila Serpa](https://br.linkedin.com/in/kamila-serpa)

## Tutorial
https://blog.wgbn.com.br/criando-bibliotecas-reutiliz%C3%A1veis-para-seus-projetos-angular-7f9576c5ad2b
 - Criando biblioteca compartilhável entre projetos:
    `ng g library ng-exames --prefix=is`
 - "Para usar a nossa biblioteca em outro aplicativo externo, vamos ter que gerar um arquivo de build do NPM, um arquivo que pega o seu build da biblioteca e compacta num arquivo padrão do NPM."
    `ng build ng-exames`
 - Empacotamento
    `cd dist/ng-exames`
    `npm pack`
 - Com isso NPM deve gerar com sucesso um arquivo chamado ng-exames-0.0.1.tgz dentro da pasta dist/ng-exames, onde o -0.0.1 é a versão que você definiu no arquivo Library project package.json.
 - Para adicionar em outro projeto rode npm install "path_do_arquivo.tgz"
  `npm install "[PATH-DA-BIBLIOTECA/dist/ng-exames/ng-exames-0.0.1.tgz" --save`
 - Pode ser necessário copiar a pasta dist/ng-exames para dentro da pasta dist de um terceiro projeto
   - package.json:  "ng-exames": "file:.dist/ng-exames"
   - import { NgExamesModule } from '../../../.dist/ng-exames';
 - O arquivo `public-api.ts` exporta os arquivos.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
