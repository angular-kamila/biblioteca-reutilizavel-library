import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgExamesModule } from 'projects/ng-exames/src/public-api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgExamesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
